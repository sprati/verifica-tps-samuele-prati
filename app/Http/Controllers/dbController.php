<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

class dbController extends Controller
{
    public function addVideo (Request $request) {

        // imposta i valori da inserire
        $title = $request->input('title');
        $description = $request->input('descsription');

        // esegue la query insert che inserisce i valori desiderati nella tabella selezionata
        $result = app('db')->insert("INSERT INTO videos
        (title, description, insertDate, editDate) VALUES ('$title', '$description', now(), now())");

        // risposta lato client
        return new Response(null, 201);
    }

    public function deleteVideo (Request $request, $id) {

        // esegue la query delete che elimina la tupla con l'idi corrispondete nella tabella selezionata
        $result = app('db')->delete("DELETE FROM videos WHERE id=$id");

        return new Response(null, 204);
    }





    public function showUser (Request $request, $id) {

        // esegue la query select seleziona gli elementi da una tabella
        $result = app('db')->select("SELECT * FROM videos WHERE id=$id");

        if ($id == 0)
            return new Response('user not found', 404);
        else
            return new Response($result, 200);
    }

    public function updateUser (Request $request, $id) {

        $fullname = $request->input('fullname');
        $email = $request->input('email');
        $city = $request->input('city');
        $birthday = $request->input('birthday');

        // esegue la query update va a modificare i valori presenti in una determinata tupla in una tabella
        //$result = app('db')->update("UPDATE users SET fullname='$fullname', email='$email', city='$city', birthday=new date($birthday)");
        $result = app('db')->update("UPDATE users SET fullname='$fullname', email='$email', city='$city'");

        return new Response(null, 200);
    }


    public function addView (Request $request, $id_user, $id_video) {

        $result = app('db')->insert("INSERT INTO views (video_id, user_id, viewDate) VALUES ($id_video, $id_user, now())");
        return new Response(null, 201);
    }
}
