<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// video
$router->get('/video', function () use ($router) {

    $result = app('db')->select("SELECT * FROM videos");
    return $result;
});

// richiamano le funzioni del controller

$router->post('/video', 'dbController@addVideo');

$router->delete('/video/{id}', 'dbController@deleteVideo');

// user
$router->get('/user/{id}', 'dbController@showUser');

$router->put('/user/{id}', 'dbController@updateUser');

// view
$router->post('/view/{id_user}/{id_video}', 'dbController@addView');
