var divApp = document.getElementById("app");

// aspetta fino a che tutto non è carico poi esegue il main
document.addEventListener("DOMContentLoaded", main);

async function deleteVideo(id) {
    await fetch('/video/' + id, {
        method: 'DELETE',
        headers: {'Content-Type':'application/json'}
    });

    render();
}

async function main() {

    // creazione dell'elemento html
    //----------------------------------------------------
    var ulList = document.createElement('ul');
    ulList.id = "List";
    divApp.appendChild(ulList);

    // esegue il rendere che permette di caricare senza aggiornare la pagina
    //----------------------------------------------------
    render();
}

// quando eseguito permette di modificare la pagina senza aggiornarla
async function render() {

    // server per ascoltare e eseguire le richieste online [n.b senza specificare il metodo usa di sefault il get]
    var Response = await fetch('/video');
    // trasforma in formato json il ritorno della lista
    var Json = await Response.json();

    let ulList = document.getElementById('List');
    // svuota la lista
    ulList.innerHTML = "";

    // riempie la lista
    for (var i = 0; i < Json.length; i++) {

        var liElement = document.createElement('li');
        var spanElement = document.createElement('span');
        var removeButton = document.createElement('button');
        removeButton.innerText = "X";

        var video = Json[i];

        // informazioni per la rimozione
        var removeVideo = deleteVideo.bind(null, video.id);

        // funzione che si avia alla pressione del tasto "X"
        removeButton.addEventListener('click', removeVideo);


        // formattazione degli elementi della lista
        spanElement.textContent = ' #' + video.id + ' - ' + video.title + ' [ ' +video.description + ' ]';

        // carica gli elementi sull'html
        liElement.appendChild(removeButton);
        liElement.appendChild(spanElement);
        ulList.appendChild(liElement);
    }

}


